﻿using FortisPayWeb_Sandbox.ViewModels;
using System.Web.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Web;

namespace FortisPayWeb_Sandbox.Controllers
{
    public class PaymentsController : Controller
    {
        public ActionResult MakePayment()
        {
            ViewModels.CustomData txnDetails = new ViewModels.CustomData();

            if (this.Request.QueryString.Count > 0)
            {
                txnDetails.PatientName = this.Request.QueryString["patientName"];
                txnDetails.PatientEmail = this.Request.QueryString["patientEmail"];
                txnDetails.TransactionAmount = this.Request.QueryString["paymentAmount"];
            }

            FortisPay_v2 Hpp   = new FortisPay_v2();
            txnDetails.Id = Hpp.id;
            txnDetails.Json = "{\"id\":\"" + Hpp.id + "\"}";
            var converted = JsonConvert.SerializeObject(txnDetails.Id);
            txnDetails.Data = HttpUtility.UrlEncode( FortisPay_v2.EncryptData(txnDetails.Json, "10i1xC20jskYCoLCchoX7xvnwaLM5dnH") );

            return View(txnDetails);
        }
    }
}