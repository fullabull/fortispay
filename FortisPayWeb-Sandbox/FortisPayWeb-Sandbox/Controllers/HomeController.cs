﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FortisPayWeb_Sandbox.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //if (this.Request.QueryString["paymentAmount"] != "")
            if (this.Request.QueryString.Keys.Count > 0)
                return RedirectToRoute(new { controller = "Payments", action = "MakePayment" });
            else
                return View();
        }

    }
}