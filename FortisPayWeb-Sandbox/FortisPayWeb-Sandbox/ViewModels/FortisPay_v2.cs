﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Linq;
using System.Text;

namespace FortisPayWeb_Sandbox.ViewModels
{
    public class FortisPay_v2
    {
        [JsonProperty("id")]
        public string id = "11eca93b17081e4a9dcb6004";

        public static byte[] encrypted;

        public static byte[] jtKey = new byte[32];
        public static byte[] jtIV = new byte[16];

        public static string EncryptData(string original, string Encryptionkey) 
        {
            byte[] EncryptionKeyByteArray = System.Text.Encoding.ASCII.GetBytes(Encryptionkey);
            byte[] salt = new byte[8];
            try
            {
                //
                // first, generate a secure salt
                //
                using (var a = new RNGCryptoServiceProvider())
                    a.GetBytes(salt);

                byte[] salted = new byte[0];
                byte[] dx = new byte[0];
                while(salted.Length < 48)
                {
                    MD5 md5 = System.Security.Cryptography.MD5.Create();
                    var r = new MemoryStream();
                    r.Write(dx, 0, dx.Length);
                    r.Write(EncryptionKeyByteArray, 0, EncryptionKeyByteArray.Length);
                    r.Write(salt, 0, salt.Length);
                    byte[] saltShaker = r.ToArray();
                    dx = md5.ComputeHash(saltShaker);
                    var s = new MemoryStream();
                    s.Write(salted, 0, salted.Length);
                    s.Write(dx, 0, dx.Length);
                    salted = s.ToArray();
                    System.Diagnostics.Debug.WriteLine(salted);
                }

                //
                // Next, create the key and iv needed to encrypt
                //
                Buffer.BlockCopy(salted, 0, jtKey, 0, 32 );
                Buffer.BlockCopy(salted, 32, jtIV, 0, 16 );

                //
                // Now, create a new instance of the Aes class.
                // This generates a new key and initialization vector (IV).
                //
                using (Aes myAes = Aes.Create())
                {
                    // Encrypt the string to an array of bytes.
                    encrypted = EncryptStringToBytes_Aes(original, jtKey, jtIV);

                    // Decrypt the bytes to a string.
                    string roundtrip = DecryptStringFromBytes_Aes(encrypted, jtKey, jtIV);

                    //Display the original data and the decrypted data.
                    System.Diagnostics.Debug.WriteLine("Original:   " + original);
                    System.Diagnostics.Debug.WriteLine("Round Trip: " + roundtrip);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error: " + e.Message);
            }

            byte[] noise = Encoding.ASCII.GetBytes("Salted__");
            byte[] returnThis = noise.Concat(salt).Concat(encrypted).ToArray();
            return Convert.ToBase64String(returnThis);
        }

        static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            byte[] encrypted;

            // Create an Aes object with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key,
                                                                    aesAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt,
                                                                     encryptor,
                                                                     CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }

                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            // Return the encrypted bytes from the memory stream.
            return encrypted;
        }

        static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold the decrypted text.
            string plaintext = null;

            // Create an Aes object with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key,
                                                                    aesAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt,
                                                                     decryptor,
                                                                     CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }

            return plaintext;

        }
    }
}