﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FortisPayWeb_Sandbox.ViewModels
{
    public class CustomData
    {
        public string TransactionAmount { get; set; } = "43.21";
        public string PatientName { get; set; } = "Johnboy Walton";
        public string PatientEmail { get; set; } = "ABC123@Motown.com";
        public string Id = "11eca93b17081e4a9dcb6004";
        public string Json = "";
        public string Data = "";
    }
}