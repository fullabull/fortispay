﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace FortisPayWeb_Sandbox.ViewModels
{
    public class FortisPay
    {
        [JsonProperty("id")]
        public string id = "11eca93b17081e4a9dcb6004";

        [JsonProperty("field_configuration")]
        public FieldConfiguration fieldConfiguration;

        private Body _body;
        private Field _field;

        public FortisPay(string transactionAmount)
        {
            _field = new Field();
            _field.id = "transaction_amount";
            _field.value = transactionAmount;
            _field.label = "Amount";
            _field.@readonly = true;
            _field.visible = true;

            _body = new Body();
            _body.Fields = new List<Field> { };
            _body.Fields.Add(_field);

            fieldConfiguration = new FieldConfiguration();
            fieldConfiguration.Body = _body;
        }

        public static string EncryptData(string textData, string Encryptionkey)
        {
            RijndaelManaged objrij = new RijndaelManaged();
            //set the mode for operation of the algorithm   
            objrij.Mode = CipherMode.CBC;
            //set the padding mode used in the algorithm.   
            objrij.Padding = PaddingMode.PKCS7;
            //set the size, in bits, for the secret key.   
            objrij.KeySize = 0x80;
            //set the block size in bits for the cryptographic operation.    
            objrij.BlockSize = 0x80;
            //set the symmetric key that is used for encryption & decryption.    
            byte[] passBytes = Encoding.UTF8.GetBytes(Encryptionkey);
            //set the initialization vector (IV) for the symmetric algorithm    
            byte[] EncryptionkeyBytes = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

            int len = passBytes.Length;
            if (len > EncryptionkeyBytes.Length)
            {
                len = EncryptionkeyBytes.Length;
            }
            Array.Copy(passBytes, EncryptionkeyBytes, len);

            objrij.Key = EncryptionkeyBytes;
            objrij.IV = EncryptionkeyBytes;

            //Creates a symmetric AES object with the current key and initialization vector IV.    
            ICryptoTransform objtransform = objrij.CreateEncryptor();
            byte[] textDataByte = Encoding.UTF8.GetBytes(textData);
            //Final transform the test string.  
            return Convert.ToBase64String(objtransform.TransformFinalBlock(textDataByte, 0, textDataByte.Length));
        }
    }

    public class FieldConfiguration
    {
        [JsonProperty("body")]
        public Body Body { get; set; }
    }

    public class Body
    {
        [JsonProperty("fields")]
        public List<Field> Fields { get; set; }
    }

    public class Field
    {
        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("value")]
        public string value { get; set; }

        [JsonProperty("label")]
        public string label { get; set; }

        [JsonProperty("readonly")]
        public bool @readonly { get; set; }

        [JsonProperty("visible")]
        public bool visible { get; set; }
    }

}